﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class SpriteLink
{
    public string name;
    public Sprite sprite;

    public SpriteLink(string name, Sprite sprite)
    {
        this.name = name;
        this.sprite = sprite;
    }

}

public class NextIconHolder : MonoBehaviour {

    public Sprite defaultSprite;
    public List<SpriteLink> spriteLinks;

    private Dictionary<string, Sprite> _imageDictionary;
    private Image image;

	// Use this for initialization
	void Start () {
        image = GetComponentsInChildren<Image>()[1];

        _imageDictionary = new Dictionary<string, Sprite>();

        spriteLinks.ForEach(link =>
        {
            _imageDictionary.Add(link.name, link.sprite);
        });

	}

    public void SwitchImage(string name)
    {
        if (!_imageDictionary.ContainsKey(name))
        {
            Debug.LogWarning("No key " + name + " found in the dictionary");
            image.sprite = defaultSprite;
        }
        else
        {
            image.sprite = _imageDictionary[name];
        }
    }
}
