﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToCamera : MonoBehaviour {

    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
    }

    // Update is called once per frame
    void Update () {
        transform.rotation = _camera.transform.rotation;
        //transform.Rotate(new Vector3(0.0f, 0.0f, 180.0f));
	}
}
