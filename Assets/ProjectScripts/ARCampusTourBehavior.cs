﻿using SimpleJSON;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Xml;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class ARCampusTourBehavior : MonoBehaviour {

    [SerializeField]
    public GameObject _uiHolder;

    public GameObject _nextIconHolder;

    [SerializeField]
    private Camera _camera;

    [SerializeField]
    private Button _button;

    [SerializeField]
    private Text _debugText;

    public GameObject _landmarkMarker;
    public GameObject _nothingMarker;

    private string[] _debugStrings;
    private bool _captured;

    private bool _locationInitialized;

    private float _deviceLat, _deviceLon;

    private string _filePath = "sdcard/ARTour";

    private Vector3 _cameraPos, _cameraFor;
    private Quaternion _cameraRot;

    private List<GameObject> _ARLabelObjects;

    private Vector2 _tourCentre = new Vector2(53.262390f, -6.222443f);

    private string _serverURL = "http://670d440b.ngrok.io";
    private string _url (string path)
    {
        return _serverURL + path;
    }

    public Dropdown _dropdown;

    private JSONNode _tourData;
    private JSONNode _currentTourData;

    private TourProgressManager _progressManager;

    // Use this for initialization
    void Start ()
    {

        _locationInitialized = false;
        _captured = false;

        _debugStrings = new string[15];
        _debugText.text = "";
        _button.onClick.AddListener(_TakeScreenshot);
        _debugStrings[0] = "Initialised";
        _debugStrings[4] = "No saved screenshot yet";

        Input.location.Start(5.0f, 5.0f);

        if (!Directory.Exists(_filePath))
        {
            Directory.CreateDirectory("sdcard/ARTour");
        }

        _cameraPos = _camera.transform.position;
        _cameraFor = _camera.transform.forward;
        _cameraRot = _camera.transform.rotation;

        _ARLabelObjects = new List<GameObject>();

        Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);

        StartCoroutine(_GetTourData());
    }

    IEnumerator _GetTourData()
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(_url("/api/tours"));
        webRequest.timeout = 10;
        _debugStrings[5] = "Sending api request";
        yield return webRequest.SendWebRequest();
        Console.WriteLine(webRequest.responseCode);
        if (webRequest.isNetworkError)
        {
            _debugStrings[5] = "Error: " + webRequest.error;
        }
        else if (webRequest.responseCode >= 400 && webRequest.responseCode <= 599)
        {
            _debugStrings[5] = "HTTP Error, Code: " + webRequest.responseCode.ToString();
        }
        else
        {
            string data = webRequest.downloadHandler.text;
            Console.WriteLine(data);
            _debugStrings[5] = data;
            _tourData = JSON.Parse(data);
            ARCampusTourInfo._SetTourData(JSON.Parse(data));

            _dropdown.ClearOptions();
            List<Dropdown.OptionData> optionDataList = new List<Dropdown.OptionData>
            {
                new Dropdown.OptionData("Select Tour...")
            };

            for (int i = 0; i < _tourData["tours"].AsArray.Count; i++)
            {
                _debugStrings[5] = _tourData["tours"][i]["name"];
                optionDataList.Add(new Dropdown.OptionData( _tourData["tours"][i]["name"] ));
            }
            _dropdown.AddOptions(optionDataList);
            _dropdown.onValueChanged.AddListener(delegate {
                DropdownOnChanged(_dropdown);
            });
        }
    }

    void DropdownOnChanged(Dropdown dropdown)
    {
        int selectedIndex = dropdown.value - 1;
        if (selectedIndex >= 0)
        {
            _currentTourData = _tourData["tours"][selectedIndex];
            JSONNode data = ARCampusTourInfo._GetTourData();
            ARCampusTourInfo._SetCurrentTourData(data["tours"][selectedIndex]);
            _debugStrings[4] = _currentTourData["name"];
            _debugStrings[5] = "Component Exists: " + (GetComponent<ARMapBehaviour>() != null);
            GetComponent<ARMapBehaviour>()._UpdateMapCenter();
            GetComponent<ARMapBehaviour>()._SetMapButtonInteracable(true);

            List<string> attractionNames = new List<string>();
            JSONArray attractionJSON = _currentTourData["attractions"].AsArray;
            for (int i = 0; i < attractionJSON.Count; i++)
            {
                attractionNames.Add(attractionJSON[i]["attraction_name"]);
            }

            _progressManager = new TourProgressManager(attractionNames);

            if (_nextIconHolder != null && _nextIconHolder.GetComponent<NextIconHolder>() != null)
            {
                _nextIconHolder.GetComponent<NextIconHolder>().SwitchImage(attractionNames[0]);
            }
            
        }
        else
        {
            _debugStrings[4] = "None selected";
            GetComponent<ARMapBehaviour>()._SetMapButtonInteracable(false);
        }
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        _locationInitialized = Input.location.status == LocationServiceStatus.Running;

        _debugStrings[1] = string.Format("Camera rotation: ({0,0:f2}, {1,0:f2}, {2,0:f2})", _camera.transform.rotation.eulerAngles.x, _camera.transform.rotation.eulerAngles.y, _camera.transform.rotation.eulerAngles.z);

        if (!_locationInitialized)
        {
            _debugStrings[2] = "Location services not available. Using Trinity's Location";
            _deviceLat = 53.343864f;
            _deviceLon = -6.254518f;
        }
        else
        {
            _deviceLat = Input.location.lastData.latitude;
            _deviceLon = Input.location.lastData.longitude;
            _debugStrings[2] = "Current location: " + _deviceLat.ToString() + ", " + _deviceLon.ToString();
        }

        _debugStrings[14] = GetComponent<ARMapBehaviour>()._GetDebugString();
        _debugText.text = string.Join("\n", _debugStrings);

        if (_ARLabelObjects.Capacity > 0)
        {
            _ARLabelObjects.ForEach(obj =>
            {
                obj.transform.rotation = _camera.transform.rotation; 
            });
        }
        
    }

    void _TakeScreenshot ()
    {
        //if (_locationInitialized)
        //{
            if (!Directory.Exists(_filePath))
            {
                Directory.CreateDirectory(_filePath);
            }

            _debugStrings[2] = "Taking screenshot";
            string screenshotName = "Screenshot__" + System.DateTime.Now.ToString("yyyy-MM-dd-mm-ss") + ".jpg";
            string screenshotInfoName = "ScreenshotInfo__" + System.DateTime.Now.ToString("yyyy-MM-dd-mm-ss") + ".xml";
            string screenshotPath = System.IO.Path.Combine(_filePath, screenshotName);
            string screenshotInfoPath = System.IO.Path.Combine(_filePath, screenshotInfoName);

            _cameraPos = _camera.transform.position;
            _cameraFor = _camera.transform.forward;

            StartCoroutine(_processCapture(screenshotPath, screenshotName, screenshotInfoPath, screenshotInfoName));

        //}
    }

    IEnumerator _processCapture(string file, string filename, string info, string infoname)
    {
        _uiHolder.SetActive(false);

        yield return new WaitForEndOfFrame();

        _debugStrings[6] = "Taking screenshot";

        _captureScreenshotAsJPG(file);
        _GetImageInformation(info);

        _debugStrings[6] = "Finished screenshot";

        yield return new WaitForEndOfFrame();

        Handheld.StartActivityIndicator();

        yield return StartCoroutine(_UploadFiles(file, filename, info, infoname));

        Handheld.StopActivityIndicator();

        yield return new WaitForEndOfFrame();

        _uiHolder.SetActive(true);
    }

    void _captureScreenshotAsJPG(string filepath)
    {
        Texture2D screenshot = new Texture2D(Screen.width, Screen.height);
        screenshot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        byte[] screenshotJPG = screenshot.EncodeToJPG();
        FileStream screenshotFile = File.Create(filepath);
        screenshotFile.Write(screenshotJPG, 0, screenshotJPG.Length);
        screenshotFile.Close();
    }

    void _GetImageInformation(string filepath)
    {
        XmlWriter xmlWriter = XmlWriter.Create(filepath);
        xmlWriter.WriteStartElement("info");
        xmlWriter.WriteStartElement("gps");
        xmlWriter.WriteElementString("gps_lat", _deviceLat.ToString());
        xmlWriter.WriteElementString("gps_lon", _deviceLon.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteStartElement("pose");
        xmlWriter.WriteElementString("pose_x", _camera.transform.rotation.eulerAngles.x.ToString());
        xmlWriter.WriteElementString("pose_y", _camera.transform.rotation.eulerAngles.y.ToString());
        xmlWriter.WriteElementString("pose_z", _camera.transform.rotation.eulerAngles.z.ToString());
        xmlWriter.WriteEndElement();
        xmlWriter.WriteEndElement();
        xmlWriter.Flush();
        xmlWriter.Close();
    }

    IEnumerator _UploadFiles(string file, string filename, string info, string infoname)
    {
        WWWForm form = new WWWForm();

        try
        {
            byte[] imageBytes = File.ReadAllBytes(file);
            _debugStrings[11] = "Converted image file to byte strings";
            byte[] infoBytes = File.ReadAllBytes(info);
            _debugStrings[11] = "Converted info file to byte strings";


            form.AddBinaryData("image", imageBytes, filename, "image/jpg");
            form.AddBinaryData("info", infoBytes, infoname, "application/octet-stream");
        }
        catch (Exception e)
        {
            _debugStrings[12] = e.ToString();
        }
        
        UnityWebRequest request = UnityWebRequest.Post(_url("/image"), form);
        request.timeout = 10;
        _debugStrings[11] = "Sending request. Awaiting Response.";
        yield return request.SendWebRequest();
        print("request completed with code: " + request.responseCode);
        if (request.isNetworkError)
        {
            _debugStrings[11] = request.error;
        }
        else if (request.responseCode >= 500 && request.responseCode < 600)
        {
            _debugStrings[11] = "Server Error: " + request.responseCode;
        }
        else
        {
            //_debugStrings[11] = "Request Response: " + request.responseCode.ToString();
            _debugStrings[11] = request.downloadHandler.text;
            JSONNode responseData = JSON.Parse(request.downloadHandler.text);

            //string testResp = "{\"response\":{\"center\":{\"x\": 0.5, \"y\": 0.5}, \"name\": \"Sphere Within Sphere\", \"display_name\": \"Sphere\\nWithin\\nSphere\"}}";
            //JSONNode responseData = JSON.Parse(testResp);
            Debug.Log(responseData.ToString());

            if (responseData["response"].IsString && responseData["response"] == "None")
            {
                _spawnNotFoundMessage();
                //_spawnText(0.5f, 0.5f, "Sphere Within Sphere", "Sphere\nWithin\nSphere"); // TESTING ONLY; REMOVE IN BUILD
            }
            else
            {
                float ratioX = responseData["response"]["center"]["x"].AsFloat;
                float ratioY = responseData["response"]["center"]["y"].AsFloat;
                string name = responseData["response"]["name"];
                string display = responseData["response"]["display_name"];
                _spawnText(ratioX, ratioY, name, display);
            }
        }
    }

    void _spawnText(float screenRatioX, float screenRatioY, string name, string display)
    {
        _debugStrings[13] = "Spawning text object";
        GameObject marker = Instantiate(_landmarkMarker);

        TextMesh textMesh = marker.GetComponentInChildren<TextMesh>();
        textMesh.text = display;
        //textMesh.text = name;

        marker.transform.position = _cameraPos + _cameraFor * 0.5f;
        marker.transform.rotation = _camera.transform.rotation;
        marker.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

        _ARLabelObjects.Add(marker);

        _progressManager.CheckAs(name, true);
        string nextLandmark = _progressManager.GetCurrentLandmark();

        if (_nextIconHolder != null && _nextIconHolder.GetComponent<NextIconHolder>() != null)
        {
            _nextIconHolder.GetComponent<NextIconHolder>().SwitchImage(nextLandmark);
        }

        _debugStrings[13] = "Test object spawned";
    }

    void _spawnNotFoundMessage()
    {
        _debugStrings[13] = "Spawner Not Found Marker";
        GameObject marker = Instantiate(_nothingMarker);

        marker.transform.position = _cameraPos + _cameraFor * 0.5f;
        marker.transform.rotation = _camera.transform.rotation;
        marker.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);

        Destroy(marker, 10.0f);
    }

    public float _GetDeviceLat()
    {
        return _deviceLat;
    }

    public float _GetDeviceLon()
    {
        return _deviceLon;
    }

    public Vector3 _GetCameraPose()
    {
        return _camera.transform.rotation.eulerAngles;
    }

    public bool _IsLandmarkChecked(string name)
    {
        return _progressManager.IsChecked(name);
    }
}
