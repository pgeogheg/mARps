﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Unity.Map;

public class MapZoomSlider : MonoBehaviour {

    [SerializeField]
    AbstractMap _ARMap;

    [SerializeField]
    AbstractMap _nonARMap;

    [SerializeField]
    Slider slider;

    // Use this for initialization
    void Start () {
        slider.onValueChanged.AddListener(_SetMapZoom);
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    void _SetMapZoom(float zoom)
    {
        _ARMap.SetZoom(zoom);
        _ARMap.ResetMap();

        _nonARMap.SetZoom(zoom);
        _nonARMap.ResetMap();
    }
}
