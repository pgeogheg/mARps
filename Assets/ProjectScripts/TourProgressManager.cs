﻿using System.Collections.Generic;

public class TourProgressManager {

    private List<string> landmarkNames;
    private List<bool> landmarkChecks;

    public TourProgressManager(string[] landmarknames)
    {
        landmarkNames = new List<string>();
        landmarkChecks = new List<bool>();
        for (int i = 0; i < landmarknames.Length; i++)
        {
            landmarkNames.Add(landmarknames[i]);
            landmarkChecks.Add(false);
        }
    }

    public TourProgressManager(List<string> landmarknames)
    {
        landmarkNames = new List<string>();
        landmarkChecks = new List<bool>();
        landmarknames.ForEach(name =>
        {
            landmarkNames.Add(name);
            landmarkChecks.Add(false);
        });
    }

    public bool CheckAs(string name, bool check)
    {
        if (!landmarkNames.Contains(name)) return false;

        int checkIndex = landmarkNames.IndexOf(name);
        landmarkChecks[checkIndex] = check;
        return true;
    }

    public bool IsChecked(string name)
    {
        if (!landmarkNames.Contains(name)) return false;

        int checkIndex = landmarkNames.IndexOf(name);
        return landmarkChecks[checkIndex];
    }

    public float GetProgress()
    {
        int noUnchecked = landmarkChecks.FindAll(delegate(bool b) { return !b; }).Count;
        return (float)noUnchecked / (float)landmarkChecks.Count;
    }

    public string GetCurrentLandmark()
    {
        int nextIndex = landmarkChecks.IndexOf(false);
        if (nextIndex == -1) return "";
        else return landmarkNames[nextIndex];
    }
}
