﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenNARMap : MonoBehaviour {

    [SerializeField]
    GameObject _nonARObject;

	// Use this for initialization
	void Start () {
        GetComponent<Button>().onClick.AddListener(_ToggleNARObject);

        _nonARObject.SetActive(false);
        Color newBlue = new Color();
        ColorUtility.TryParseHtmlString("#016FB9", out newBlue);
        GetComponentInChildren<Text>().color = newBlue;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void _ToggleNARObject()
    {
        bool active = _nonARObject.activeInHierarchy;

        if (!active)
        {
            _nonARObject.SetActive(true);
            GetComponentInChildren<Text>().color = Color.black;
        }
        else
        {
            _nonARObject.SetActive(false);
            Color newBlue = new Color();
            ColorUtility.TryParseHtmlString("#016FB9", out newBlue);
            GetComponentInChildren<Text>().color = newBlue;
        }
    }
}
