﻿using SimpleJSON;

public static class ARCampusTourInfo
{
    private static JSONNode _tourData;
    private static JSONNode _currentTourData;

    public static JSONNode _GetTourData()
    {
        return _tourData;
    }

    public static void _SetTourData(JSONNode newData)
    {
        _tourData = newData;
    }

    public static JSONNode _GetCurrentTourData()
    {
        return _currentTourData;
    }

    public static void _SetCurrentTourData(JSONNode newData)
    {
        _currentTourData = newData;
    }
}

