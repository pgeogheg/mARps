﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class Attraction
{
    public string attraction_name;
    public int[] gps;
    public int id;
}

[Serializable]
public class Tour
{
    public Attraction[] attractions;
    public int id;
    public string name;
}

[Serializable]
public class MetaTour
{
    public int id;
    public string name;
}

[Serializable]
public class MetaTourList
{
    public MetaTour[] metaTours;
}
