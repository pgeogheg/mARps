﻿using Mapbox.Directions;
using Mapbox.Platform;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System;
using System.Collections.Generic;

[Serializable]
public class MaterialLink
{
    public Material _material;
    public string _name;

    public MaterialLink(Material material, string name)
    {
        this._material = material;
        this._name = name;
    }
}

public class ARMapBehaviour : MonoBehaviour {

    private readonly FileSource fileSource = new FileSource("pk.eyJ1IjoicGdlb2doZWciLCJhIjoiY2pleTZjbTM1MTVpdTMwcDRsc2V1eTV4bSJ9.3Wp-UoxBRs31770m_jcCRQ");

    public Camera _camera;

    public Button _mapButton;

    private AbstractMap _map;

    private GameObject _mapObject;
    public GameObject _mapPreface;

    private Vector2d _userLoc;
    private GameObject _userObject;
    public GameObject _userPreface;

    private List<GameObject> _ARMarkers;
    private List<Vector2d> _landmarkLocations;
    public GameObject _ARMarkerPreface;

    private List<Vector2d> _routeNodes;
    private List<GameObject> _routeLines;

    private Vector2d _mapCentre;
    private bool _isActive = false;

    private string[] _debugStrings;

    public List<MaterialLink> _landmarkicons;
    private Dictionary<string, Material> _materialDictionary;

    private List<string> _landmarkNames;

	// Use this for initialization
	void Start ()
    {
        _ARMarkers = new List<GameObject>();
        _landmarkLocations = new List<Vector2d>();
        _routeNodes = new List<Vector2d>();
        _routeLines = new List<GameObject>();
        _landmarkNames = new List<string>();

        _materialDictionary = new Dictionary<string, Material>();

        _landmarkicons.ForEach(icon =>
        {
            _materialDictionary.Add(icon._name, icon._material);
        });

        _SetInactive();
        _mapButton.onClick.AddListener(_OnMapButtonPress);
        _mapButton.interactable = false;

        _debugStrings = new string[15];
	}

    public void _SetMapButtonInteracable(bool active)
    {
        _mapButton.interactable = active;
    }

    public string _GetDebugString()
    {
        return string.Join("\n", _debugStrings);
    }

    private void _OnMapButtonPress ()
    {
        _isActive = !_isActive;
        _EditButtonColours();

        if (_isActive) _SetActive();
        else _SetInactive();
    }

    private void _SetActive()
    {
        _mapObject = Instantiate(_mapPreface);
        _map = _mapObject.GetComponentInChildren<AbstractMap>();
        Vector3 forward = _camera.transform.forward;
        forward.Set(forward.x, 0.0f, forward.z);
        Vector3 finalMapPos = _camera.transform.position + forward * 0.3f;
        finalMapPos.Set(finalMapPos.x, _camera.transform.position.y, finalMapPos.z);
        _mapObject.transform.position = finalMapPos;

        _GetLandmarkPoints();
        _GetUserLocation();
        //_GetDirections();
    }

    private void _SetInactive()
    {
        _ARMarkers.Clear();
        _landmarkLocations.Clear();
        Destroy(_mapObject);
    }

    private void _EditButtonColours()
    {
        if (_isActive) _mapButton.GetComponentInChildren<Text>().color = Color.blue;
        else _mapButton.GetComponentInChildren<Text>().color = Color.black;
    }

    public void _UpdateMapCenter()
    {
        JSONNode data = ARCampusTourInfo._GetCurrentTourData();
        Vector2d tourCentre = new Vector2d(data["tour_centre"]["lat"].AsFloat, data["tour_centre"]["lon"].AsFloat);
        _mapCentre = tourCentre;
    }

    public void _GetLandmarkPoints()
    {
        _ARMarkers.Clear();
        _landmarkLocations.Clear();

        _debugStrings[1] = "Getting Landmarks";

        JSONNode data = ARCampusTourInfo._GetCurrentTourData();
        JSONArray attractions = data["attractions"].AsArray;

        _debugStrings[2] = "Attractions size: " + attractions.Count;

        _landmarkNames.Clear();

        for (int i = 0; i < attractions.Count; i++)
        {
            Vector2d location = new Vector2d(attractions[i]["gps"]["lat"], attractions[i]["gps"]["lon"]);
            _landmarkLocations.Add(location);

            GameObject newMarker = Instantiate(_ARMarkerPreface);
            newMarker.transform.parent = _mapObject.transform;
            newMarker.transform.position = _map.GeoToWorldPosition(location);

            string attraction_name = attractions[i]["attraction_name"];
            _landmarkNames.Add(attraction_name);

            if (!GetComponent<ARCampusTourBehavior>()._IsLandmarkChecked(attraction_name))
            {
                newMarker.GetComponentsInChildren<MeshRenderer>()[2].enabled = false;
            }
            
            if (_materialDictionary.ContainsKey(attraction_name))
            {
                Material landmarkIcon = _materialDictionary[attraction_name];
                newMarker.GetComponentInChildren<MeshRenderer>().material = landmarkIcon;
            }

            _debugStrings[3] += "Location " + (i + 1) + ": " + location.ToString() + " => " + newMarker.transform.position.ToString() + "\n";

            _ARMarkers.Add(newMarker);
        }

    }

    private void Update()
    {
        _debugStrings[0] = "Markers: " + _ARMarkers.Count;
        _debugStrings[3] = "";

        if (_mapObject != null)
        {
            for (int i = 0; i < _ARMarkers.Count && _map.isActiveAndEnabled; i++)
            {
                _ARMarkers[i].transform.position = _map.GeoToWorldPosition(_landmarkLocations[i]);

                _debugStrings[3] += "Location " + (i + 1) + ": " +
                    _landmarkLocations[i].ToString() + " => " +
                    _ARMarkers[i].transform.position.ToString();
            }

            for (int i = 0; i < _routeLines.Count; i++)
            {
                _routeLines[i].transform.position = _map.GeoToWorldPosition(_routeNodes[i]);
                _routeLines[i].GetComponent<LineRenderer>().SetPosition(0, _map.GeoToWorldPosition(_routeNodes[i]));
                _routeLines[i].GetComponent<LineRenderer>().SetPosition(1, _map.GeoToWorldPosition(_routeNodes[i + 1]));
                _routeLines[i].transform.parent = _mapObject.transform;
                _routeLines[i].transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
            }

            _UpdateUserLoc();
            _userObject.transform.position = _map.GeoToWorldPosition(_userLoc);
            float cameraRot = GetComponent<ARCampusTourBehavior>()._GetCameraPose().y + 180.0f;
            _userObject.transform.rotation = Quaternion.Euler(0.0f, cameraRot, 0.0f);
        }
    }

    void _GetDirections()
    {
        Directions directions = new Directions(fileSource);
        DirectionResource directionResource = new DirectionResource(_landmarkLocations.ToArray(), RoutingProfile.Walking);
        directions.Query(directionResource, _HandleDirectionResponse);
    }

    void _HandleDirectionResponse(DirectionsResponse response)
    {
        response.Routes.ForEach(route => {
            for (int i = 0; i < route.Geometry.Count; i++)
            {
                Vector2d geometry = route.Geometry[i];
                _routeNodes.Add(geometry);

                if (i < route.Geometry.Count - 1)
                {
                    Vector3 lineStart = _map.GeoToWorldPosition(geometry);
                    Vector3 lineEnd = _map.GeoToWorldPosition(route.Geometry[i + 1]);
                    _routeLines.Add(_GetRouteSegmentLine(lineStart, lineEnd));
                }
            }
        });
    }

    GameObject _GetRouteSegmentLine(Vector3 startNode, Vector3 endNode)
    {
        GameObject line = new GameObject();
        line.transform.position = startNode;
        line.AddComponent<LineRenderer>();
        LineRenderer renderer = line.GetComponent<LineRenderer>();
        renderer.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        renderer.startColor = Color.blue;
        renderer.endColor = Color.blue;
        renderer.startWidth = 0.1f;
        renderer.endWidth = 0.1f;
        renderer.SetPosition(0, startNode);
        renderer.SetPosition(1, endNode);
        return line;
    }

    void _GetUserLocation()
    {
        float userLat = GetComponent<ARCampusTourBehavior>()._GetDeviceLat();
        float userLon = GetComponent<ARCampusTourBehavior>()._GetDeviceLon();

        _userLoc = new Vector2d(userLat, userLon);

        _userObject = Instantiate(_userPreface);
        _userObject.transform.position = _map.GeoToWorldPosition(_userLoc);
        _userObject.transform.parent = _mapObject.transform;
    }

    void _UpdateUserLoc()
    {
        float userLat = GetComponent<ARCampusTourBehavior>()._GetDeviceLat();
        float userLon = GetComponent<ARCampusTourBehavior>()._GetDeviceLon();

        _userLoc = new Vector2d(userLat, userLon);
    }

    public bool MaterialExists(string name)
    {
        return _materialDictionary.ContainsKey(name);
    }

    public Material GetMaterial(string name)
    {
        return _materialDictionary[name];
    }
}
