﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TourCamera : MonoBehaviour {

    private bool camAvailable;
    private WebCamTexture deviceBackCam;
    private Texture defaultBackground;

    public RawImage background;
    public AspectRatioFitter fit;

	// Use this for initialization
	void Start () {
        defaultBackground = background.texture;
        WebCamDevice[] devices = WebCamTexture.devices;

        if (devices.Length == 0)
        {
            Debug.Log("No camera devices found");
            camAvailable = false;
            return;
        }

        for (int i = 0; i < devices.Length; i++)
        {
            if (!devices[i].isFrontFacing)
            {
                deviceBackCam = new WebCamTexture(devices[i].name, Screen.width, Screen.height);
            }
        }

        if (deviceBackCam == null)
        {
            Debug.Log("No back camera found");
            camAvailable = false;
            return;
        }

        deviceBackCam.Play();
        background.texture = deviceBackCam;

        camAvailable = true;
	}
	
	// Update is called once per frame
	void Update () {
        if (!camAvailable) return;

        float ratio = (float)deviceBackCam.width / (float)deviceBackCam.height;
        fit.aspectRatio = ratio;

        float scaleY = deviceBackCam.videoVerticallyMirrored ? -1.0f : 1.0f;
        background.rectTransform.localScale = new Vector3(1.0f, scaleY, 1.0f);

        int orient = -deviceBackCam.videoRotationAngle;
        background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);
	}
}
