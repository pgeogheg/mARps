﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mapbox.Directions;
using Mapbox.Geocoding;
using Mapbox.Platform;
using Mapbox.Utils;
using Mapbox.Unity.Map;
using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;

public class SearchForLocation : MonoBehaviour {

    private FileSource fileSource = new FileSource("pk.eyJ1IjoicGdlb2doZWciLCJhIjoiY2pleTZjbTM1MTVpdTMwcDRsc2V1eTV4bSJ9.3Wp-UoxBRs31770m_jcCRQ");

    [SerializeField]
    Text _debugText;

    [SerializeField]
    Text _inputField;

    [SerializeField]
    Text _outputField;

    [SerializeField]
    Button _activationButton;

    [SerializeField]
    AbstractMap _ARMap;

    [SerializeField]
    AbstractMap _nonARMap;

    [SerializeField]
    GameObject _mapObjectHolder;

    Vector2d _location;
    Vector2d _here;
    List<Vector2d> _routeNodes;
    List<GameObject> _routeLines;

    List<GameObject> _nonARRouteLines;

    ILocationProvider _locationProvider;

    private bool active = false;

    // Use this for initialization
    void Start () {
        active = false;
        _activationButton.onClick.AddListener(_PerformSearch);
        _routeNodes = new List<Vector2d>();
        _routeLines = new List<GameObject>();
        _nonARRouteLines = new List<GameObject>();
        _debugText.text = "DEBUG:\n";
        _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
    }
	
	// Update is called once per frame
	void Update () {
		if (active)
        {
            _UpdateRouteSegmentLines();
        }
    }

    void _PerformSearch()
    {
        var locationString = _inputField.text;
        _outputField.text = "Loading " + locationString + "....";
        Geocoder geocoder = new Geocoder(fileSource);
        ForwardGeocodeResource geocodeResource = new ForwardGeocodeResource(locationString);
        geocoder.Geocode(geocodeResource, _LoadSearchResults);
    }

    void _LoadSearchResults(ForwardGeocodeResponse response)
    {
        _location = response.Features[0].Center;
        _outputField.text = "Found: " + _location.ToString();
        _GetDirections();
        active = true;
    }

    void _GetDirections()
    {
        _debugText.text += "Getting Waypoints\n";
        Directions directions = new Directions(fileSource);
        _here = _locationProvider.CurrentLocation.LatitudeLongitude;
        Vector2d[] vector2Ds = { _here, _location };
        _debugText.text += "From: " + _here.ToString() + "\n";
        _debugText.text += "Destination: " + _location.ToString() + "\n";
        DirectionResource directionResource = new DirectionResource(vector2Ds, RoutingProfile.Walking);
        _debugText.text += "Loaded request\n";    
        directions.Query(directionResource, _HandleDirectionResponse);
    }

    void _HandleDirectionResponse(DirectionsResponse response)
    {
        _debugText.text += "Routes Found: " + response.Routes.Count + "\n";
        response.Routes.ForEach( route => {
            _debugText.text += "Route Nodes Found: " + route.Geometry.Count + "\n";
            for (int i = 0; i < route.Geometry.Count; i++)
            {
                Vector2d geometry = route.Geometry[i];
                _routeNodes.Add(geometry);

                _debugText.text += "Route Node: " + geometry.ToString() + "\n";

                if (i < route.Geometry.Count - 1)
                {
                    Vector3 lineStart = _ARMap.GeoToWorldPositionXZ(geometry);
                    Vector3 lineEnd = _ARMap.GeoToWorldPositionXZ(route.Geometry[i + 1]);
                    _routeLines.Add(_GetRouteSegmentLine(lineStart, lineEnd));

                    _nonARRouteLines.Add(_GetRouteSegmentLine(new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, 0.0f, 0.0f))); 
                }
            }
        });
    }

    GameObject _GetRouteSegmentLine(Vector3 startNode, Vector3 endNode)
    {
        GameObject line = new GameObject();
        line.transform.position = startNode;
        line.AddComponent<LineRenderer>();
        LineRenderer renderer = line.GetComponent<LineRenderer>();
        renderer.material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        renderer.startColor = Color.blue;
        renderer.endColor = Color.blue;
        renderer.startWidth = 0.1f;
        renderer.endWidth = 0.1f;
        renderer.SetPosition(0, startNode);
        renderer.SetPosition(1, endNode);
        return line;
    }

    void _UpdateRouteSegmentLines()
    {
        for (int i = 0; i < _routeLines.Count; i++)
        {
            _routeLines[i].transform.position = _ARMap.GetComponent<AbstractMap>().GeoToWorldPositionXZ(_routeNodes[i]);
            _routeLines[i].GetComponent<LineRenderer>().SetPosition(0, _ARMap.GetComponent<AbstractMap>().GeoToWorldPositionXZ(_routeNodes[i]));
            _routeLines[i].GetComponent<LineRenderer>().SetPosition(1, _ARMap.GetComponent<AbstractMap>().GeoToWorldPositionXZ(_routeNodes[i + 1]));

            if (_nonARMap.gameObject.activeInHierarchy)
            {
                _nonARRouteLines[i].SetActive(true);
                _nonARRouteLines[i].transform.position = _nonARMap.GetComponent<AbstractMap>().GeoToWorldPosition(_routeNodes[i]);
                _nonARRouteLines[i].GetComponent<LineRenderer>().SetPosition(0, _nonARMap.GetComponent<AbstractMap>().GeoToWorldPosition(_routeNodes[i]));
                _nonARRouteLines[i].GetComponent<LineRenderer>().SetPosition(1, _nonARMap.GetComponent<AbstractMap>().GeoToWorldPosition(_routeNodes[i + 1]));
            }
            else
            {
                _nonARRouteLines[i].SetActive(false);
            }
        }
    }
}
